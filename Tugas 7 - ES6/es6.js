// 1. Mengubah fungsi menjadi fungsi arrow
// Normal Javascript
const golden = function goldenFunction() {
    console.log("this is golden!!")
}

golden()

//ES6
const golden2 = goldenFunction2 = () => {
    console.log("this is golden2!")
}

golden2()

// 2. Sederhanakan menjadi Object literal di ES6
// Normal Javascript
const newFunction = function literal(firstName, lastName) {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function() {
            console.log(firstName + " " + lastName)
            return
        }
    }
}

// Driver Code
newFunction("William", "Imoh").fullName()

//ES6
const newFunction2 = literal2 = (firstName2, lastName2) => {
    return {
        firstName2, lastName2, fullName2 : function() {
            console.log(`${firstName2} ${lastName2}`)
        }
    }
}

// Driver Code
newFunction2("William2", "Imoh2").fullName2()

// 3. Destructuring
const newObject = {
    firstName3: "Harry",
    lastName3: "Potter Holt",
    destination2: "Hogwarts React Conf",
    occupation2: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const firstName = newObject.firstName3;
const lastName = newObject.lastName3;
const destination = newObject.destination2;
const occupation = newObject.occupation2;

console.log(firstName, lastName, destination, occupation)

const { firstName3, lastName3, destination2, occupation2, spell } = newObject;

console.log(newObject)

// 4. Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)

//Driver Code
console.log(combined)
let combinedArray = [...west, ...east]
console.log(combinedArray)

// 5. Template Literals
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(before)

var before2 = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before2)