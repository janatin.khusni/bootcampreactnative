import React from 'react'
import { StyleSheet, Text, View, Button, Image } from 'react-native'

export default function Login({navigation}) {
    return (
        <View style={styles.container}>
            <Image
                style={styles.avatarDrawwer}
                source={require('./asset/logo-twitter.png')}
            />
            <Text>Login</Text>
            <Button onPress={()=>navigation.navigate("HomeScreen")} 
            title="Menuju Halaman HomeScreen" />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center'
    },
    avatarDrawwer:{
        height:12,
        width: 18
    }
})