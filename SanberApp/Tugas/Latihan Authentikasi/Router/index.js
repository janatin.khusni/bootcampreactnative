import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import AboutScreen from '../AboutMe';
import HomeScreen from '../Home';
import LoginScreen from '../Login';
import RegisScreen from '../Register';
import ApiScreen from '../Tugas-14-RestApi';

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Router() {
    return (
      <NavigationContainer>
          <Stack.Navigator headerMode="none">
              <Stack.Screen name="LoginScreen" component={LoginScreen}/>
              <Stack.Screen name="HomeScreen" component={HomeScreen}/>
              <Stack.Screen name="RegisScreen" component={RegisScreen}/>
              {/* <Stack.Screen name="AboutScreen" component={AboutScreen}/> */}
              <Stack.Screen name="MainApp" component={MainApp}/>
              <Stack.Screen name="MyDrawwer" component={MyDrawwer}/>
          </Stack.Navigator>
      </NavigationContainer>
    )
}

const MainApp =()=>(    
        <Tab.Navigator>
            <Tab.Screen name="HomeScreen" component={HomeScreen}/>
            <Tab.Screen name="ApiScreen" component={ApiScreen}/>
        </Tab.Navigator>    
)

const MyDrawwer =()=>(
    <Drawwer.Navigator>
        <Drawwer.Screen name="App" component={MainApp}/>
        <Drawwer.Screen name="AboutScreen" component={AboutScreen} />
    </Drawwer.Navigator>
)