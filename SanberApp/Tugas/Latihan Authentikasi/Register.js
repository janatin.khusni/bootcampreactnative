import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Button } from 'react-native'
import * as firebase from 'firebase'

export default function Register({navigation}) {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const firebaseConfig = {
        apiKey: "AIzaSyC5-kRHca2vxNq-_IAE_vlPKOps5QAvRw8",
        authDomain: "sanber-1e28a.firebaseapp.com",
        projectId: "sanber-1e28a",
        storageBucket: "sanber-1e28a.appspot.com",
        messagingSenderId: "436059631477",
        appId: "1:436059631477:web:cdf8f3d58e3beb65bced3c",
        measurementId: "G-NMVQDGEQTP"
    };
    if(!firebase.apps.length){
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    }

    const submit = () => {
        const data = {
            email, password
        }
        console.log(data)
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(()=>{
            console.log("Register Berhasil")
            navigation.navigate("MyDrawwer",{
                screen : 'App', params:{
                    screen:'HomeScreen'
                }
            })
        }).catch(()=>{
            console.log("Register Gagal")
        })
    }

    return (
        <View style={styles.container}>
            <View style={{alignItems:'center'}}>
                {/* <Text style={styles.title}>About Me</Text> */}
                <Text style={styles.logo}>ABOUT ME</Text>
            </View>

            <View style={{flexDirection: 'column', alignItems:'center'}}>
                <View style={{alignItems:'flex-start',flexDirection: 'column'}}>
                    <Text>Username/Email</Text>
                    <TextInput 
                        value={email}
                        onChangeText={(value)=>setEmail(value)}
                        placeholder="" 
                        style={styles.input}
                    ></TextInput>
                </View>

                <View style={{alignItems:'flex-start', flexDirection: 'column', marginTop:20}}>
                    <Text>Password</Text>
                    <TextInput 
                        value={password}
                        onChangeText={(value)=>setPassword(value)}
                        placeholder="" 
                        style={styles.input}
                    ></TextInput>
                </View>
            </View>

            <View style={{alignItems:'center'}}>
                <TouchableOpacity                     
                    onPress={submit}
                    style={styles.buttonRegistrasi}>
                    <Text style={{color:'#4B0107', fontSize:18}}>REGISTER</Text>
                </TouchableOpacity>

                <Text style={{padding:20}}>Atau</Text>

                <TouchableOpacity
                    onPress={()=>navigation.navigate("LoginScreen")} 
                    style={styles.buttonWhite}>
                    <Text style={{color:'#4B0107', fontSize:18}}>LOGIN</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        justifyContent: 'space-evenly',
        flex: 1
    },
    title:{
        color: 'black',
        fontSize: 40,
    },
    logo:{
        color: 'black',
        fontSize: 20,
        borderRadius: 10,
        padding:10,
        borderColor:'#BA3232',
        borderWidth: 2
    },
    buttonRegistrasi:{
        backgroundColor: '#FE5059',
        width: 240,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:10
    },
    buttonWhite:{
        backgroundColor: 'white',
        width: 240,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:10,
        borderColor:'#FE5059',
        borderWidth:3
    },
    input:{
        backgroundColor:'#FACECF',
        padding:10,
        width: 300,
        borderColor:'#FE5059'
    }
})