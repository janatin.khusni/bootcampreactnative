import React, { useEffect, useState } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import * as firebase from 'firebase'

export default function Home({navigation}) {
    const [user, setUser] = useState({})

    useEffect(() => {
        const userInfo = firebase.auth().currentUser
        setUser(userInfo)
    }, [])
    
    const logout = () => {
        firebase.auth().signOut()
        .then(()=>{
            navigation.navigate("LoginScreen")
        })
    }

    return (
        <View style={styles.container}>
            <Text>Halaman Home</Text>
            <Text>Hello, {user.email}</Text>

            <Button
                onPress={logout}
                title="Logout"
            ></Button>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
