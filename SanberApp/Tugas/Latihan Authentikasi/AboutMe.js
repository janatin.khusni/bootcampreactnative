import React from 'react'
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Image } from 'react-native'
import * as firebase from 'firebase'

export default function AboutMe({navigation}) {
    const logout = () => {
        firebase.auth().signOut()
        .then(()=>{
            navigation.navigate("LoginScreen")
        })
    }
    return(
        <View>
            <View style={styles.container}>
                <Image
                    style={styles.avatarDrawwer}
                    source={require('./asset/image1.jpg')}
                />
                <View style={{alignItems:'center', flex:1}}>
                    <Text style={{color:'#4B0107',fontSize: 25}}>Katniss Everdeen</Text>
                    <Text style={{color:'#4B0107',fontSize: 25}}>Android Dev</Text>
                </View>
            </View>

            <View style={{padding:20}}>
                <Text style={styles.title}>Media Sosial</Text>
                <View style={{justifyContent: 'space-evenly', flexDirection:'row', marginTop:20}}>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-facebook.png')}
                        />
                        <Text style={{color:'#4B0107',fontSize: 16}}>Katniss_deen</Text>
                    </View>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-twitter.png')}
                        />
                        <Text style={{color:'#4B0107',fontSize: 16}}>@katniss_deen</Text>
                    </View>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-instagram.png')}
                        />
                        <Text style={{color:'#4B0107',fontSize: 16}}>@katniss_deen</Text>
                    </View>
                </View>
            </View>

            <View style={{padding:20}}>
                <Text style={styles.title}>Portofolio</Text>
                <View style={{justifyContent: 'space-evenly', flexDirection:'row', marginTop:20}}>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-bitbucket.png')}
                        />
                        <Text style={{color:'#4B0107',fontSize: 16}}>@katniss_deen</Text>
                    </View>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-github.png')}
                        />
                        <Text style={{color:'#4B0107',fontSize: 16}}>@katniss_deen</Text>
                    </View>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-gitlab.png')}
                        />
                        <Text style={{color:'#4B0107',fontSize: 16}}>@katniss_deen</Text>
                    </View>
                </View>
            </View>

            <TouchableOpacity style={styles.button}>
                <Text style={{color:'#4B0107', fontSize:18}}>LIHAT DAFTAR SKILL SAYA</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#FF9A9E',
        alignItems:'center',
        flexDirection:'row',
        padding:40
    },
    avatarDrawwer:{
        height:100,
        width: 100,
        borderTopEndRadius:60,
        borderTopLeftRadius:60,
        borderColor:'white',
        borderWidth:3,
        alignItems:'center'
    },
    title:{
        color: '#BA3232',
        fontSize: 20,
        fontSize:30
    },
    logo:{
        height: 68,
        width: 68,
        alignItems:'center'
    },
    button:{
        backgroundColor: '#FE5059',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:10,
        margin:40
    }
})