import axios from 'axios'
import React, {useEffect, useState} from 'react'
import {Button, StyleSheet, TextInput, View, Text, FlatList, TouchableOpacity, Alert} from 'react-native'

const Item = ({title, value, onPress, onDelete}) => {
    return (
        <View>
            <TouchableOpacity onPress={onPress}>
                <View>
                    <Text>{title}</Text>
                    <Text>{value}</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={onDelete}>
                <Text>X</Text>
            </TouchableOpacity>            
        </View>
    )
}

const LocalAPI = () => {
    const [title, setTitle] = useState("");
    const [value, setValue] = useState("");
    const [users, setUsers] = useState([])
    const [button, setButton] = useState("Simpan")
    const [selected, setSelected] = useState({})

    useEffect(() => {
        getData();
    }, [])

    const submit = () => {
        const data = {
            title, 
            value
        }
        if(button === 'Simpan'){
            axios.post('https://achmadhilmy-sanbercode.my.id/api/v1/news',data)
            .then(res => {
                console.log('res: ',res);
                setTitle("")
                setValue("")
                getData();
            })
        }else{
            axios.put(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${selected.id}`, data)
            .then(res => {
                console.log('res update: ',res);
                setTitle("")
                setValue("")
                getData();
                setButton("Simpan")
            })
        }
        
    }

    const getData = () => {
        axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
        .then(res => {
            console.log('res get Data: ', res.data.data)
            setUsers(res.data.data);
        })
    }

    const selectItem = (item) => {
        console.log('selected item: ',item)
        setTitle(item.title)
        setValue(item.value)
        setButton("Update")
        setSelected(item)
    }

    const deleteItem = (item) => {
        console.log('delete item: ',item)
        axios.delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${item.id}`)
        .then(res => {
            console.log('delete item: ',res)
            getData();
        })
    }

    return (
        <View>
            <Text>Local API (JS)</Text>
            <Text>Masukan Anggota Kabayan Coding</Text>
            <TextInput placeholder="title" value={title} onChangeText={(value) => setTitle(value)}></TextInput>
            <TextInput placeholder="value" value={value} onChangeText={(value) => setValue(value)}></TextInput>
            <Button title={button} onPress={submit}/>
            <View/>
            {users.map(user => {
                return <Item 
                    key={user.id} 
                    title={user.title} 
                    value={user.value} 
                    onPress={() => selectItem(user)}
                    // onDelete={() => deleteItem(user)}
                    onDelete={() => Alert.alert(
                        'Peringatan', 
                        'Anda yakin akan menghapus data ini?',
                        [
                            {
                                text: 'Tidak', 
                                onPress: () => console.log('button tidak')
                            }, 
                            {
                                text: 'Ya', 
                                onPress: () => deleteItem(user)
                            }
                        ]
                    )}
                />
            })}
            {/* <FlatList
                data={users}
                renderItem={renderItem}
               keyExtractor={(item) => item.id.toString()}
            /> */}
        </View>
    )
}

export default LocalAPI;