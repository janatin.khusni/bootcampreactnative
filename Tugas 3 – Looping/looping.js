// No. 1 Looping While
var flag = 2;
console.log("LOOPING PERTAMA");

while(flag <= 20){
    console.log( flag + " - I love coding" );
    flag += 2;
}

console.log("LOOPING KEDUA");
flag -= 2;

while(flag >= 2){
    console.log( flag + " - I will become a mobile developer" );
    flag -= 2;
}

// No. 2 Looping menggunakan for
for(flag = 1; flag <= 20; flag++){
    if(flag%3 == 0 && flag%2 == 1){
        console.log(flag + " - I Love Coding");
    }else if(flag%2 == 1){
        console.log(flag + " - Santai")
    }else if(flag%2 == 0){
        console.log(flag + " - Berkualitas");
    }
}

// No. 3 Membuat Persegi Panjang #
var baris = 4;
do {
    var pagar = "";
    var deret = 8;
    while(deret > 0){
        pagar += "#";
        deret--;
    }
    console.log(pagar);
    baris--;
} while (baris > 0);

// No. 4 Membuat Tangga
for(var baris = 0; baris < 7; baris++){
    var pagar = "";
    var deret = baris + 1;
    do {
        pagar += "#";
        deret--;
    } while (deret > 0);
    console.log(pagar);
}

// No. 5 Membuat Papan Catur
baris = 0;
while(baris < 8){
    var pagar = "";
    for(var deret = 0; deret < 8; deret++){
        if (baris%2 == deret%2){
            pagar += " ";
        }else{
            pagar += "#";
        }
    }
    console.log(pagar);
    baris++;
}