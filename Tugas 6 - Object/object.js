function batas(){
    console.log()
    console.log("=========================")
    console.log()
}

var now = new Date()
var thisYear = now.getFullYear()    // 2021 (tahun sekarang)

function arrayToObject(arr) {
    // code di sini
    // for(var urut = 0; urut < arr; urut++){

    // }
    if(arr == ""){
        console.log()

        batas()
    }else{
        for(var urut = 0; urut < 2; urut++){
            var person = arr[urut]

            if (person != null){
                var age
                if (person[3] == null || person[3] > thisYear) {
                    age = "Invalid Birth Year"
                }else{
                    age = thisYear - person[3]
                }

                var bruceObj = {
                    firstName : person[0],
                    lastName : person[1],
                    gender : person[2],
                    age : age
                }

                var car = {
                    brand : "Ferrari",
                    type : "Sports Car",
                    price : 50000000,
                    "horse power" : 986
                }
                // console.log(bruceObj)
                
                var no = urut + 1
                var fullName = bruceObj.firstName + " " + bruceObj.lastName
                
                console.log(no + ". " + fullName + ": ")
                console.log(bruceObj)

                batas()
            }
        }
    }
}

// Drive Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

// Error case 
arrayToObject([]) // ""

// Soal No. 2 (Shopping Time)
var sale = [ ["Sepatu Stacattu", 1500000], ["Baju Zoro", 500000], ["Baju H&N", 250000], ["Sweater Uniklooh", 175000], ["Casing Handphone", 50000] ]

function shoppingTime(memberId, money){
    if (memberId == null || memberId == "") {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money <= 50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        var listPurchased = [];
        for(var urutSale = 0; urutSale < 5; urutSale++){
            if (sale[urutSale][1] <= money) {
                listPurchased.push(sale[urutSale][0])
                money -= sale[urutSale][1]
            }
        }

        var shopping = {
            "memberId" : memberId,
            "money" : money,
            "listPurchased" : listPurchased,
            "changeMoney" : money
        }

        return shopping;
    }
}

console.log(shoppingTime("324193hDew2", 700000))
console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

batas()

// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrayNaikAngkot = []
    // your code here
    if(arrPenumpang == ""){
        return arrayNaikAngkot
    }

    for(var urut = 0; urut < 2; urut++){
        var angkotArr = arrPenumpang[urut]
        var bayar = 0
        var rute1 = 0
        var rute2 = 0

        for(var urutRute = 0; urutRute < rute.length; urutRute++){
            if (angkotArr[1] == rute[urutRute]) {
                rute1 = urutRute 
            }

            if (angkotArr[2] == rute[urutRute]) {
                rute2 = urutRute 
            }
        }

        if (rute2 < rute1) {
            bayar = 2000 * (rute1 - rute2)
        }else if (rute1 < rute2) {
            bayar = 2000 * (rute2 - rute1)
        }


        var naikAngkotObj = {
            "penumpang" : angkotArr[0],
            "naikDari" : angkotArr[1],
            "tujuan" : angkotArr[2],
            "bayar" : bayar
        }

        arrayNaikAngkot.push(naikAngkotObj)
    }

    return arrayNaikAngkot
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));

batas()

console.log(naikAngkot([])); //[]

batas()
