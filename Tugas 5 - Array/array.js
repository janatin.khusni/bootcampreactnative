// Soal No. 1 (Range)
function range(startNum, finishNum){
    if(startNum == null || finishNum == null){
        return -1
    }else{
        var angka = []
        if(startNum < finishNum){
            while(startNum <= finishNum){
                angka.push(startNum)
                startNum++
            }
        } else {
            for(startNum; startNum >= finishNum; startNum--){
                angka.push(startNum)
            }
        }
        return angka;
    }
}

console.log("Soal No. 1 (Range)")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("=========================")
console.log()

// Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    if(startNum == null || finishNum == null || step == null){
        return "Parameter ada yang kurang"
    }else{
        var angka = []
        if(startNum < finishNum){
            while(startNum <= finishNum){
                angka.push(startNum)
                startNum+=step
            }
        } else {
            for(startNum; startNum >= finishNum; startNum-=step){
                angka.push(startNum)
            }
        }
        return angka;
    }
}

console.log("Soal No. 2 (Range with Step)")
console.log(rangeWithStep(1, 10, 2))    // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3))   // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1))     // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4))    // [29, 25, 21, 17, 13, 9, 5]
console.log("=========================")
console.log()

// Soal No. 3 (Sum of Range)
function sum(startNum, endNum, step){
    var angka;
    if (step == null){
        angka = range(startNum, endNum);
    }else{
        angka = rangeWithStep(startNum, endNum, step);
    }

    if(angka.length == null){
        if (startNum == null){
            return 0
        }else {
            return startNum
        }
    } else {
        var hasil = 0;
        for(var urut = 0; urut < angka.length; urut++){
            hasil += angka[urut]
        }
        return hasil;
    }
}

console.log("Soal No. 3 (Sum of Range)")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log("=========================")
console.log()

// Soal No. 4 (Array Multidimensi)
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(input, urut){
    console.log("Nomor ID:  " + input[urut][0])
    console.log("Nama Lengkap:  " + input[urut][1])
    console.log("TTL:  " + input[urut][2])
    console.log("Hobi:  " + input[urut][3])
}

console.log("Soal No. 4 (Array Multidimensi)")
dataHandling(input, 0)
console.log()
dataHandling(input, 1)
console.log()
dataHandling(input, 2)
console.log()
dataHandling(input, 3)
console.log("=========================")
console.log()

// Soal No. 5 (Balik Kata)
function balikKata(kata){
    var balik = "";
    for(var urut = 0; urut < kata.length; urut++){
        balik += kata[kata.length - urut - 1]
    }
    return balik
}

console.log("Soal No. 5 (Balik Kata)")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("=========================")
console.log()

// Soal No. 6 (Metode Array)
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

function dataHandling2(input){
    input[1] += "Elsharawy"
    input[2] = "Provinsi " + input[2]
    input.splice(4,1,"Pria", "SMA Internasional Metro")

    var lahir = input[3].split("/")
    var bulan
    switch (lahir[1]) {
        case "01": { bulan = "Januari"; break; }
        case "02": { bulan = "Februari"; break; }
        case "03": { bulan = "Maret"; break; }
        case "04": { bulan = "April"; break; }
        case "05": { bulan = "Mei"; break; }
        case "06": { bulan = "Juni"; break; }
        case "07": { bulan = "Juli"; break; }
        case "08": { bulan = "Agustus"; break; }
        case "09": { bulan = "September"; break; }
        case "10": { bulan = "Oktober"; break; }
        case "11": { bulan = "November"; break; }
        case "12": { bulan = "Desember"; break; }
        default: { bulan = "Tidak ada"; break; }
    }
    var lahirDesc = lahir.sort(function (value1, value2) { return value2 - value1 } ) ;
    var lahirJoin = lahir.join("-")

    var nama = input[1].split(" ")
    var nama2 = "";
    for(var urut = 0; urut < nama.length; urut++){
        var nama3 = ""
        if (nama2.length == 0){
            nama3 = nama[urut];
        }else{
            nama3 = nama2 + " " + nama[urut];
        }
        if (nama3.length <= 15){
            nama2 = nama3
        }
    }

    console.log("Soal No. 6 (Metode Array)")
    console.log(input)
    console.log(bulan)
    console.log(lahirDesc)
    console.log(lahirJoin)
    console.log(nama2)
    console.log("=========================")
    console.log()
}