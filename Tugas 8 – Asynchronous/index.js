// di index.js
let readBooks = require('./callback.js')

let books = [
    {name : 'LOTR', timeSpent : 3000},
    {name : 'Fidas', timeSpent : 2000},
    {name : 'Kalkulus', timeSpent : 4000}
]

let waktu = 10000
let bukuKe = 0;

appFunction = () => {
    readBooks(waktu, books[bukuKe], function (sisaWaktu) {
        waktu = sisaWaktu
        bukuKe++;
        if (bukuKe == 3) {
            bukuKe = 0;
        }

        if(sisaWaktu > books[bukuKe].timeSpent) {
            appFunction()
        }
    })
}

appFunction()