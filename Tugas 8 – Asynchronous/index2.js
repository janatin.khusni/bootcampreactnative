let readBooksPromise = require('./promise.js')

let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let waktu = 100000
let bukuKe = 0;

appFunction = () => {
    readBooksPromise(waktu, books[bukuKe]).then(function (fulfilled) {
        waktu -= books[bukuKe].timeSpent
        bukuKe++;
        
        if (bukuKe == 3) {
            bukuKe = 0;
        }
        
        appFunction()
    }).catch(function (error) {
        
    });
}

appFunction()