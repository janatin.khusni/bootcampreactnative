// Animal Class
// Release 0
class Animal {
    // Code class di sini
    constructor(brand){
        this.name = brand
        this.legs = 4
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
class Ape extends Animal{
    constructor(brand) {
        super(brand)
        this.name = brand;
    }
    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal{
    constructor(brand) {
        super(brand)
        this.name = brand;
    }
    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell()    // "Auooo"

var kodok = new Frog("buduk")
kodok.jump()    // hop hop

// Function to Class
function Clock({ template }) {
    var timer;
    
    function render() {
        var date = new Date();
        var hours = date.getHours();
        
        if (hours < 10) hours = '0' + hours;
        
        var mins = date.getMinutes();
        
        if (mins < 10) mins = '0' + mins;
        
        var secs = date.getSeconds();
        
        if (secs < 10) secs = '0' + secs;
        
        console.log(template);
        var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
        
        console.log(output);
    }
  
    this.stop = function() {
        clearInterval(timer);
    };
  
    this.start = function() {
        render();
        timer = setInterval(render, 1000);
    };
}
  
var clock = new Clock({template: 'h:m:s'});
// clock.start(); 

// // function di atas diubah menjadi struktur class seperti berikut:
class Clock2 {
    // Code di sini
    constructor(template){
        this.template = template.template
        this.timer = 0
    }

    render(){
        var date = new Date();
        var hours = date.getHours();
        
        if (hours < 10) hours = '0' + hours;
        
        var mins = date.getMinutes();
        
        if (mins < 10) mins = '0' + mins;
        
        var secs = date.getSeconds();
        
        if (secs < 10) secs = '0' + secs;
   
        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
        
        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    };

    start() {
        this.render()
        // this.timer = setInterval(this.render.bind(this), 1000)   // ini bisa
        this.timer = setInterval(() => this.render(), 1000)         // ini juga bisa
    };
}
  
var clock2 = new Clock2({template: 'h:m:s'});
clock2.start();